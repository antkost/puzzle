### Visit project:

http://far-airplane.surge.sh/

### To start the project:

```
1. cd into project
2. yarn
3. yarn start
```

### To run tests:

`yarn test` or `yarn test:coverage`

### Stack that has been used:

- React (create-react-app)
- styled components
- CSS grid
- react-testing-library
- react-beautiful-dnd

### Further improvements:

[ ] - make mobile / tablet friendly;

[ ] - add ability to stop the game and see the answer;

[ ] - upload your own image;

[ ] - change difficulty level (number of pieces);

[ ] - refactor timer with rxjs;

[ ] - add type checking (typescript);

[ ] - make possible to play with keyboard only;

[ ] - use portal for modal;
