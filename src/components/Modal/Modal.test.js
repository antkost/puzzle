import React from 'react';
import { render, fireEvent } from 'react-testing-library';

import Modal from './';

test('renders without crashing', () => {
  render(
    <Modal show={true} close={() => {}}>
      Test
    </Modal>
  );
});

test(`calls close handler on pressing 'Start again'`, () => {
  const handleClose = jest.fn();
  const { container, getByText } = render(
    <Modal show={true} close={handleClose}>
      Test
    </Modal>
  );

  const closeBtn = getByText('Start again');
  fireEvent.click(closeBtn);
  expect(handleClose).toHaveBeenCalledTimes(1);
});
