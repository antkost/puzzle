import React from 'react';
import styled from 'styled-components';

import { borderRadius, modalBorder } from '../../utils/style.constants';

const ModalContainer = styled.div`
  background: white;
  border: ${modalBorder};
  border-radius: ${borderRadius}px;
  box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 7px 20px 0 rgba(0, 0, 0, 0.17);
  font-weight: bold;
  left: 50%;
  opacity: ${props => (props.show ? 1 : 0)};
  position: fixed;
  pointer-events: ${props => (props.show ? 'auto' : 'none')};
  top: 50%;
  transform: translate(-50%, -50%);
  width: 70%;
  max-width: 600px;
`;

const ModalBody = styled.div`
  height: 400px;
  padding: 1rem 5rem;
  text-align: center;
  overflow: auto;
`;

const ModalFooter = styled.div`
  text-align: center;
  padding: 1rem;
`;

const ButtonStart = styled.button`
  background-color: white;
  border: ${modalBorder};
  border-radius: ${borderRadius}px;
  cursor: ${props => (props.show ? 'pointer' : 'auto')};
  font-weight: bold;
  font-size: 2rem;
  padding: 0.75rem;
`;

const Modal = props => {
  const { show, close, children } = props;

  return (
    <ModalContainer show={show}>
      <ModalBody>{children}</ModalBody>
      <ModalFooter>
        <ButtonStart onClick={close} show={show}>
          Start again
        </ButtonStart>
      </ModalFooter>
    </ModalContainer>
  );
};

export default Modal;
