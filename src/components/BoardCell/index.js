import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {
  pictureUrl,
  pictureHeight,
  pictureWidth,
  cardWidth,
  cardHeight
} from '../../utils/picture.constants';
import { getBackgroundPosition } from '../../utils/card.utils';

const Cell = styled.div`
  background: ${props => (props.isActive ? 'white' : 'ghostwhite')};
  background-color: ${props => (props.isDraggingOver ? 'lavender' : null)};
  border: 2px solid lightblue;
  transform: scale(${props => (props.isDraggingOver ? 0.95 : 1)});
  transition: transform 200ms ease-in-out;
  background-image: ${props => (props.isMatched ? pictureUrl : 'none')};
  background-position: ${props =>
    props.isMatched
      ? getBackgroundPosition(props.row, props.column, cardWidth, cardHeight)
      : 'unset'};
  background-size: ${pictureWidth}px ${pictureHeight}px;
`;

class BoardCell extends PureComponent {
  render() {
    const {
      innerRef,
      placeholder,
      isActive,
      matched,
      row,
      column,
      isDraggingOver
    } = this.props;

    return (
      <Cell
        innerRef={innerRef}
        isActive={isActive}
        isMatched={matched}
        row={row}
        column={column}
        isDraggingOver={isDraggingOver}
      >
        {placeholder}
      </Cell>
    );
  }
}

export default BoardCell;
