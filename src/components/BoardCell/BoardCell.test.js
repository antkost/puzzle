import React from 'react';
import { render } from 'react-testing-library';

import BoardCell from './';

test('renders without crashing', () => {
  render(<BoardCell isActive={false} matched={false} row={0} column={0} />);
});

test('renders active cell', () => {
  const { container } = render(
    <BoardCell isActive={true} matched={false} row={0} column={0} />
  );
  expect(container.firstChild).toHaveStyleRule('background', 'white');
});

test('renders background image when matched', () => {
  const { container } = render(
    <BoardCell isActive={true} matched={true} row={0} column={0} />
  );
  expect(container.firstChild).toMatchSnapshot();
});

test('renders background image when matched', () => {
  const { container } = render(
    <BoardCell isActive={true} matched={true} row={0} column={0} />
  );
  expect(container.firstChild).toMatchSnapshot();
});

test('renders styles when is dragged over', () => {
  const { container } = render(
    <BoardCell
      isActive={true}
      matched={true}
      row={0}
      column={0}
      isDraggingOver={true}
    />
  );
  expect(container.firstChild).toHaveStyleRule('background-color', 'lavender');
  expect(container.firstChild).toHaveStyleRule('transform', 'scale(0.95)');
});
