import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Time from '../Time';

import { DEFAULT_COLOR, ERROR_COLOR } from '../../utils/style.constants';

const Container = styled.div`
  text-align: center;
  margin-bottom: 20px;
`;

const TimerLabel = styled.h2`
  transition: transform 200ms ease-in-out;
`;

class Timer extends PureComponent {
  state = {
    elapsedTime: 0,
    style: { color: DEFAULT_COLOR }
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      isTimerOn,
      shouldAddTime,
      shouldResetTime,
      onTimeAdded,
      recordPlayer
    } = this.props;

    if (isTimerOn && !this.interval) {
      this.startTimer();
    }

    if (!isTimerOn && this.interval) {
      this.stopTimer();
      recordPlayer(this.state.elapsedTime);
    }

    if (shouldResetTime) {
      this.setState({ elapsedTime: 0 });
    }

    if (shouldAddTime && !prevProps.shouldAddTime) {
      this.addTime();
      onTimeAdded();
    }
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  startTimer = () => {
    this.start = new Date().getTime();

    this.interval = setInterval(() => {
      const elapsed = new Date().getTime() - this.start;
      this.setState({ elapsedTime: elapsed });
    }, 10);
  };

  stopTimer = () => {
    clearInterval(this.interval);
    this.interval = null;
  };

  addTime = () => {
    this.start = this.start - 5000;

    setTimeout(() => {
      this.setState({
        style: { color: DEFAULT_COLOR }
      });
    }, 300);
    this.setState({
      style: {
        color: ERROR_COLOR,
        transform: 'scale(2)'
      }
    });
  };

  render() {
    const { elapsedTime } = this.state;

    return (
      <Container>
        <TimerLabel style={this.state.style}>
          <Time time={elapsedTime} />
        </TimerLabel>
      </Container>
    );
  }
}

export default Timer;
