import React from 'react';
import { render, wait, waitForElement } from 'react-testing-library';

import Timer from './';
import { DEFAULT_COLOR, ERROR_COLOR } from '../../utils/style.constants';

test('renders without crashing', () => {
  const { container } = render(<Timer />);
  expect(container.textContent).toBe('0:00:000');
});

test('starts timer', async () => {
  const recordPlayerSpy = jest.fn();
  const { container, rerender } = render(
    <Timer isTimerOn={false} recordPlayer={recordPlayerSpy} />
  );
  rerender(<Timer isTimerOn={true} recordPlayer={recordPlayerSpy} />);

  const hasTimeElapsed = await waitForElement(
    () => {
      const milliseconds = container.textContent.slice(-3);
      return parseInt(milliseconds, 10) > 100;
    },
    { container }
  );
  expect(hasTimeElapsed).toBe(true);
});

test('stops timer', async () => {
  let millisecondsElapsed;
  const recordPlayerSpy = jest.fn();
  const { container, rerender } = render(
    <Timer isTimerOn={false} recordPlayer={recordPlayerSpy} />
  );
  rerender(<Timer isTimerOn={true} recordPlayer={recordPlayerSpy} />);

  const hasTimeElapsed = await waitForElement(
    () => {
      const milliseconds = container.textContent.slice(-3);
      millisecondsElapsed = parseInt(milliseconds, 10);
      return millisecondsElapsed > 100;
    },
    { container }
  );
  rerender(<Timer isTimerOn={false} recordPlayer={recordPlayerSpy} />);

  expect(hasTimeElapsed).toBe(true);
  expect(recordPlayerSpy).toHaveBeenCalledTimes(1);

  const hasTimerStopped = await waitForElement(
    () => {
      const milliseconds = container.textContent.slice(-3);
      return parseInt(milliseconds, 10) === millisecondsElapsed;
    },
    { container, timeout: 5000 }
  );

  expect(hasTimerStopped).toBe(true);
});

test('adds time', async () => {
  const onTimeAddedSpy = jest.fn();
  const { container, rerender } = render(<Timer shouldAddTime={false} />);
  const timerLabel = container.querySelector('h2');
  rerender(<Timer shouldAddTime={true} onTimeAdded={onTimeAddedSpy} />);

  expect(onTimeAddedSpy).toHaveBeenCalledTimes(1);
  expect(timerLabel.style.color).toBe(ERROR_COLOR);

  const isLabelDefault = await waitForElement(
    () => {
      return timerLabel.style.color === DEFAULT_COLOR;
    },
    { container }
  );

  expect(isLabelDefault).toBe(true);
});

test('resets time', async () => {
  let millisecondsElapsed;
  const recordPlayerSpy = jest.fn();
  const { container, rerender } = render(
    <Timer isTimerOn={false} recordPlayer={recordPlayerSpy} />
  );
  rerender(<Timer isTimerOn={true} recordPlayer={recordPlayerSpy} />);

  const hasTimeElapsed = await waitForElement(
    () => {
      const milliseconds = container.textContent.slice(-3);
      millisecondsElapsed = parseInt(milliseconds, 10);
      return millisecondsElapsed > 100;
    },
    { container }
  );

  expect(millisecondsElapsed).toBeGreaterThan(0);

  rerender(
    <Timer
      isTimerOn={false}
      recordPlayer={recordPlayerSpy}
      shouldResetTime={true}
    />
  );

  expect(container.textContent).toBe('0:00:000');
});

test('clears timeout on unmounting', () => {
  const clearIntervalSpy = jest.spyOn(window, 'clearInterval');
  const { rerender, unmount } = render(
    <Timer isTimerOn={false} recordPlayer={() => {}} />
  );
  rerender(<Timer isTimerOn={true} recordPlayer={() => {}} />);
  unmount();
  expect(clearIntervalSpy).toHaveBeenCalledTimes(1);
});
