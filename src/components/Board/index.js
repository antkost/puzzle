import React, { PureComponent } from 'react';
import styled from 'styled-components';
import { Droppable } from 'react-beautiful-dnd';

import BoardCell from '../BoardCell';
import {
  cardHeight,
  cardWidth,
  boardWidth,
  boardHeight
} from '../../utils/picture.constants';

const BoardContainer = styled.div`
  display: grid;
  grid-gap: ${props => (props.isActive ? 3 : 0)}px;
  grid-template-columns: repeat(${boardWidth}, ${cardWidth}px);
  grid-template-rows: repeat(${boardHeight}, ${cardHeight}px);
  justify-content: center;
`;

class Board extends PureComponent {
  render() {
    const { board, isActive } = this.props;

    return (
      <BoardContainer isActive={isActive}>
        {board.map(cell => (
          <Droppable key={cell.id} droppableId={cell.id}>
            {(provided, snapshot) => (
              <BoardCell
                innerRef={provided.innerRef}
                placeholder={provided.placeholder}
                {...provided.droppableProps}
                isDraggingOver={snapshot.isDraggingOver}
                isActive={isActive}
                matched={cell.matched}
                row={cell.row}
                column={cell.column}
              />
            )}
          </Droppable>
        ))}
      </BoardContainer>
    );
  }
}

export default Board;
