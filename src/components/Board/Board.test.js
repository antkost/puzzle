import React from 'react';
import { render } from 'react-testing-library';
import { DragDropContext } from 'react-beautiful-dnd';

import Board from './';
import { generateBoard } from '../../App/initialState';

test('renders cells', () => {
  const board = generateBoard(2, 2);
  const { container } = render(
    <DragDropContext>
      <Board board={board} isActive={false} />
    </DragDropContext>
  );
  expect(container.firstChild).toMatchSnapshot();
});

test('renders active board', () => {
  const { container } = render(
    <DragDropContext>
      <Board board={[]} isActive={true} />
    </DragDropContext>
  );
  expect(container.firstChild).toMatchSnapshot();
  expect(container.firstChild).toHaveStyleRule('grid-gap', '3px');
});
