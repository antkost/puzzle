import React, { PureComponent } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import styled from 'styled-components';

import { getRandomInt, getBackgroundPosition } from '../../utils/card.utils';
import {
  pictureUrl,
  cardWidth,
  cardHeight,
  pictureWidth,
  pictureHeight
} from '../../utils/picture.constants';

const Container = styled.div`
  position: absolute;
  top: ${props => props.top}px;
  left: ${props => props.left}px;
  z-index: ${props => props.zIndex};
  background-image: ${pictureUrl};
  background-size: ${pictureWidth}px ${pictureHeight}px;
  background-position: ${props =>
    getBackgroundPosition(props.row, props.column, cardWidth, cardHeight)};
  box-shadow: 0 15px 30px 0 rgba(0, 0, 0, 0.11),
    0 5px 15px 0 rgba(0, 0, 0, 0.08);
  height: ${cardHeight}px;
  width: ${cardWidth}px;
  transition: box-shadow 100ms ease-in-out, transform 100ms ease-in-out;

  &:hover {
    transform: scale(1.05);
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 7px 20px 0 rgba(0, 0, 0, 0.17);
  }
`;

class Card extends PureComponent {
  constructor(props) {
    super(props);
    const {
      position: { topMax, leftMax },
      cardsNumber
    } = this.props;

    const top = getRandomInt(topMax);
    const left = getRandomInt(leftMax);
    const zIndex = getRandomInt(cardsNumber);

    this.state = { top, left, zIndex };
  }

  render() {
    const { top, left, zIndex } = this.state;
    const { cardId, cell } = this.props;

    return (
      <Draggable draggableId={cardId} index={cardId}>
        {provided => (
          <Container
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            innerRef={provided.innerRef}
            row={cell.row}
            column={cell.column}
            top={top}
            left={left}
            zIndex={zIndex}
          />
        )}
      </Draggable>
    );
  }
}

export default Card;
