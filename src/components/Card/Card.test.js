import React from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { render } from 'react-testing-library';

import * as cardUtils from '../../utils/card.utils';
import Card from './';

test('renders without crashing', () => {
  render(
    <DragDropContext>
      <Card
        cardId="1"
        cell={{ row: 0, column: 0 }}
        cardsNumber={10}
        position={{ topMax: 10, leftMax: 10 }}
      />
    </DragDropContext>
  );
});

test('applies random top, left and zIndex', () => {
  cardUtils.getRandomInt = jest.fn(() => 8);
  const cardsNumber = 10;
  const topMax = 11;
  const leftMax = 12;
  const { container } = render(
    <DragDropContext>
      <Card
        cardId="1"
        cell={{ row: 0, column: 0 }}
        cardsNumber={cardsNumber}
        position={{ topMax, leftMax }}
      />
    </DragDropContext>
  );
  expect(cardUtils.getRandomInt).toHaveBeenCalledTimes(3);
  expect(cardUtils.getRandomInt).toHaveBeenCalledWith(cardsNumber);
  expect(cardUtils.getRandomInt).toHaveBeenCalledWith(leftMax);
  expect(cardUtils.getRandomInt).toHaveBeenCalledWith(topMax);
  expect(container.firstChild).toMatchSnapshot();
});
