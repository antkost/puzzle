import React, { PureComponent } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import styled from 'styled-components';

import Card from '../Card';

const Stack = styled.div`
  background: lightblue;
  margin-left: 20px;
  position: relative;
  width: 280px;
`;

class CardsStack extends PureComponent {
  render() {
    const { slices, board } = this.props;

    return (
      <Droppable droppableId="stack">
        {provided => (
          <Stack innerRef={provided.innerRef} {...provided.droppableProps}>
            {slices.map(slice => (
              <Card
                key={slice}
                cardId={slice}
                position={{ topMax: 420, leftMax: 80 }}
                cardsNumber={slices.length}
                cell={board.find(cell => cell.id === slice)}
              />
            ))}
          </Stack>
        )}
      </Droppable>
    );
  }
}

export default CardsStack;
