import React from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { render } from 'react-testing-library';

import CardsStack from './';
import { generateBoard, getSlices } from '../../App/initialState';
import * as cardUtils from '../../utils/card.utils';

test('renders without crashing', () => {
  render(
    <DragDropContext>
      <CardsStack slices={[]} board={[]} />
    </DragDropContext>
  );
});

test('renders cards', () => {
  const board = generateBoard(2, 2);
  const slices = getSlices(board);
  cardUtils.getRandomInt = jest.fn(() => 8);

  const { container } = render(
    <DragDropContext>
      <CardsStack slices={slices} board={board} />
    </DragDropContext>
  );
  expect(container.firstChild).toMatchSnapshot();
});
