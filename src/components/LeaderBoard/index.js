import React, { PureComponent } from 'react';
import styled from 'styled-components';

import Time from '../Time';
import { EDITING_COLOR } from '../../utils/style.constants';

export const DEFAULT_PLAYER_NAME = 'Player';

const LeaderName = styled.span`
  text-align: left;
`;

const LeaderListItem = styled.li`
  margin-bottom: 0.7rem;
`;

const LeaderTime = styled.div`
  background-color: ${props => (props.isEditing ? EDITING_COLOR : null)};
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
`;

const PlayerNameInput = styled.input`
  border: 2px solid black;
  background: transparent;
`;

const SubmitNameButton = styled.button`
  position: absolute;
  right: -50px;
  border: 2px solid black;
`;

class LeaderBoard extends PureComponent {
  state = {
    name: DEFAULT_PLAYER_NAME
  };

  onChange = e => {
    this.setState({ name: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.submit(this.state.name);
  };

  render() {
    const { leaders } = this.props;

    return (
      <div>
        <h1>Well done!</h1>
        <ol>
          {leaders.map((leader, index) => (
            <LeaderListItem key={index}>
              <LeaderTime isEditing={leader.editing}>
                {leader.editing ? (
                  <form onSubmit={this.onSubmit}>
                    <PlayerNameInput
                      type="text"
                      name="name"
                      placeholder="Your name"
                      autoFocus={true}
                      value={this.state.name}
                      onChange={this.onChange}
                    />
                  </form>
                ) : (
                  <LeaderName>{leader.name}</LeaderName>
                )}
                <Time time={leader.time} />
                {leader.editing ? (
                  <SubmitNameButton onClick={this.onSubmit}>
                    OK
                  </SubmitNameButton>
                ) : null}
              </LeaderTime>
            </LeaderListItem>
          ))}
        </ol>
      </div>
    );
  }
}

export default LeaderBoard;
