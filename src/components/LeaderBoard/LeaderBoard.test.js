import React from 'react';
import { render, fireEvent } from 'react-testing-library';

import LeaderBoard from './';
import { initialLeaders } from '../../App/initialState';

test('renders leaders without crashing', () => {
  const leaders = [...initialLeaders];
  const { container } = render(
    <LeaderBoard leaders={leaders} submit={() => {}} />
  );
  expect(container.firstChild).toMatchSnapshot();
});

describe('Editing name field', () => {
  const newLeader = { name: 'Player', editing: true, time: 111 };
  const leaders = [...initialLeaders, newLeader];

  test('renders a form and a button when in editing mode', () => {
    const { container } = render(
      <LeaderBoard leaders={leaders} submit={() => {}} />
    );
    expect(container.querySelector('ol')).toMatchSnapshot();
  });

  test('calls onSubmit with the username when submitted', () => {
    const handleSubmit = jest.fn();
    const { container, getByText } = render(
      <LeaderBoard leaders={leaders} submit={handleSubmit} />
    );
    const form = container.querySelector('form');
    const nameInput = container.querySelector('input');
    const submitButton = getByText('OK');
    nameInput.value = newLeader.name;

    fireEvent.click(submitButton);
    expect(handleSubmit).toHaveBeenCalledTimes(1);
    expect(handleSubmit).toHaveBeenCalledWith(newLeader.name);

    fireEvent.submit(form);
    expect(handleSubmit).toHaveBeenCalledTimes(2);
  });

  test('handles filling the input', () => {
    const { container } = render(
      <LeaderBoard leaders={leaders} submit={() => {}} />
    );
    const nameInput = container.querySelector('input');

    fireEvent.change(nameInput, {
      target: { value: 'Real name' }
    });

    expect(nameInput.value).toBe('Real name');
  });
});
