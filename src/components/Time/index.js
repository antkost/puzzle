import React from 'react';

import {
  getMinutes,
  getSeconds,
  getMilliSeconds
} from '../../utils/timer.utils';

const Time = props => {
  const { time } = props;

  return (
    <span>
      {getMinutes(time)}:{getSeconds(time)}:{getMilliSeconds(time)}
    </span>
  );
};

export default Time;
