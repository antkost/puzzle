import React from 'react';
import { render } from 'react-testing-library';
import Time from '.';

test('renders time when it is given', () => {
  const { container } = render(<Time time={123} />);
  expect(container.textContent).toMatch('0:00:123');
});
