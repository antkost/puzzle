export const borderRadius = 3;
export const modalBorder = '4px solid black';

export const DEFAULT_COLOR = 'black';
export const ERROR_COLOR = 'red';
export const EDITING_COLOR = 'aquamarine';
