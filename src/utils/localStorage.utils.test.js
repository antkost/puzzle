import { saveToLocalStorage, getFromLocalStorage } from './localStorage.utils';

const data = { foo: 'bar' };

test('sets key value pair to localStorage', () => {
  const localStorageSpy = jest.spyOn(localStorage, 'setItem');

  saveToLocalStorage('test', data);
  expect(localStorageSpy).toHaveBeenCalledWith('test', JSON.stringify(data));
  localStorage.removeItem('test');
});

test('gets key value pair to localStorage', () => {
  const localStorageSpy = jest.spyOn(localStorage, 'getItem');

  let result = getFromLocalStorage('test');
  expect(localStorageSpy).toHaveBeenCalled();
  expect(result).toBe(null);

  saveToLocalStorage('test', data);
  result = getFromLocalStorage('test');
  expect(result).toEqual(data);
});
