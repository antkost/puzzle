export const getMilliSeconds = milliseconds => {
  return `00${milliseconds}`.slice(-3);
};

export const getSeconds = milliseconds => {
  return `0${Math.floor(milliseconds / 1000) % 60}`.slice(-2);
};

export const getMinutes = milliseconds => {
  return Math.floor(milliseconds / 60000);
};
