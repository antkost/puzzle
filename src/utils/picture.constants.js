// import Background from '../assets/earth.jpg';
import Background from '../assets/logo.png';

export const pictureWidth = 600;
export const pictureHeight = 600;

// change difficulty level
export const boardHeight = 3;
export const boardWidth = 3;

export const cardHeight = pictureHeight / boardHeight;
export const cardWidth = pictureWidth / boardWidth;
export const pictureUrl = `url(${Background})`;
