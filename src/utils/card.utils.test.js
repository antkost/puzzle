import { getRandomInt, getBackgroundPosition } from './card.utils';

test('returns random number lower then provided limit', () => {
  const max = 10;

  expect(getRandomInt(max)).toBeLessThan(max);
  expect(Number.isInteger(getRandomInt(max))).toBe(true);
});

test('returns background-position value', () => {
  let row = 1;
  let column = 2;
  const cardWidth = 200;
  const cardHeight = 100;
  let expected = '-200px -200px';

  expect(getBackgroundPosition(row, column, cardWidth, cardHeight)).toBe(
    expected
  );

  row = 0;
  column = 0;
  expected = '0px 0px';
  expect(getBackgroundPosition(row, column, cardWidth, cardHeight)).toBe(
    expected
  );
});
