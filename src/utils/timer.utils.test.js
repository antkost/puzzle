import { getMilliSeconds, getMinutes, getSeconds } from './timer.utils';

test('returns 3-digit millisecond value', () => {
  expect(getMilliSeconds(3)).toBe('003');
  expect(getMilliSeconds(23)).toBe('023');
  expect(getMilliSeconds(123)).toBe('123');
  expect(getMilliSeconds(1234)).toBe('234');
});

test('returns 2-digit second value', () => {
  expect(getSeconds(900)).toBe('00');
  expect(getSeconds(1900)).toBe('01');
  expect(getSeconds(20000)).toBe('20');
  expect(getSeconds(123000)).toBe('03');
});

test('returns number of minutes', () => {
  expect(getMinutes(900)).toBe(0);
  expect(getMinutes(123456)).toBe(2);
});
