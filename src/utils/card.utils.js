export const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

export const getBackgroundPosition = (row, column, cardWidth, cardHeight) => {
  return `${row * -cardWidth}px ${column * -cardHeight}px`;
};
