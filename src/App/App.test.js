import React from 'react';
import ReactDOM from 'react-dom';

import App from './';
import { DEFAULT_PLAYER_NAME } from '../components/LeaderBoard';
import * as localStorageUtils from '../utils/localStorage.utils';

// These tests are rather testing an implementation details
// and not the kind of tests I would like to write.
// However achieving 100% test coverage requires testing implementation details.

describe('App instance', () => {
  const div = document.createElement('div');
  const instance = ReactDOM.render(<App />, div);
  let endGameSpy;

  beforeEach(() => {
    endGameSpy = jest.spyOn(instance, 'hasGameFinished');
  });

  afterEach(() => {
    endGameSpy.mockRestore();
    const initialState = {
      board: [],
      stack: { slices: [] }
    };
    instance.setState({ initialState });
  });

  test('start timer on dragstart', () => {
    const spy = jest.spyOn(instance, 'setState');
    instance.onDragStart();
    expect(spy).toHaveBeenCalledWith({
      isTimerOn: true,
      shouldResetTime: false
    });
    spy.mockRestore();
  });

  test('no destiation selected', () => {
    const result = { destination: null };
    instance.onDragEnd(result);
    expect(endGameSpy).not.toHaveBeenCalled();
  });

  test('same destiation selected', () => {
    const result = {
      destination: {
        droppableId: '1'
      },
      source: {
        droppableId: '1'
      }
    };
    instance.onDragEnd(result);
    expect(endGameSpy).not.toHaveBeenCalled();
  });

  test('dropped on already occupied cell', () => {
    const initialState = {
      board: [{ id: '1', column: 0, row: 0, matched: true }],
      stack: { slices: [] }
    };
    instance.setState({ initialState });
    const result = {
      destination: {
        droppableId: '1'
      },
      source: {
        droppableId: '2'
      }
    };
    instance.onDragEnd(result);
    expect(endGameSpy).not.toHaveBeenCalled();
  });

  test('dropped on a correct cell', () => {
    const goodMatchSpy = jest.spyOn(instance, 'onGoodMatch');
    const openModalSpy = jest.spyOn(instance, 'openModal');
    const initialState = {
      board: [
        { id: '1', column: 0, row: 0, matched: false },
        { id: '2', column: 0, row: 0, matched: false }
      ],
      stack: { slices: [] }
    };
    instance.setState({ initialState });

    const result = {
      destination: {
        droppableId: '1'
      },
      source: {
        droppableId: '2'
      },
      draggableId: '1'
    };

    instance.onDragEnd(result);

    expect(endGameSpy).toHaveBeenCalled();
    expect(openModalSpy).not.toHaveBeenCalled();
    expect(goodMatchSpy).toHaveBeenCalledWith(result.destination.droppableId);
    openModalSpy.mockRestore();
    goodMatchSpy.mockRestore();
  });

  test('dropped on a bad cell', () => {
    const onBadMatch = jest.spyOn(instance, 'onBadMatch');
    const initialState = {
      board: [],
      stack: { slices: [] }
    };
    instance.setState({ initialState });
    const result = {
      destination: {
        droppableId: '1'
      },
      source: {
        droppableId: '2'
      },
      draggableId: '3'
    };

    instance.onDragEnd(result);
    expect(onBadMatch).toHaveBeenCalled();
    expect(endGameSpy).not.toHaveBeenCalled();
    onBadMatch.mockRestore();
  });

  test('finish the game', () => {
    const goodMatchSpy = jest.spyOn(instance, 'onGoodMatch');
    const stopTimerSpy = jest.spyOn(instance, 'setState');
    const openModalSpy = jest.spyOn(instance, 'openModal');
    const initialState = {
      board: [{ id: '1', column: 0, row: 0, matched: true }],
      stack: { slices: [] }
    };
    instance.setState({ initialState });
    const result = {
      destination: {
        droppableId: '5'
      },
      source: {
        droppableId: '2'
      },
      draggableId: '5'
    };

    instance.onDragEnd(result);
    expect(goodMatchSpy).toHaveBeenCalledWith(result.destination.droppableId);
    expect(endGameSpy).toHaveBeenCalled();
    expect(stopTimerSpy).toHaveBeenCalledWith({ isTimerOn: false });
    expect(openModalSpy).toHaveBeenCalled();
    goodMatchSpy.mockRestore();
    stopTimerSpy.mockRestore();
    openModalSpy.mockRestore();
  });

  test('matching cell on board', () => {
    const initialState = {
      board: [
        { id: '123', column: 0, row: 0, matched: false },
        { id: '456', column: 0, row: 0, matched: false }
      ],
      stack: { slices: ['123', '456'] }
    };
    const expectedState = {
      board: [
        { id: '123', column: 0, row: 0, matched: true },
        { id: '456', column: 0, row: 0, matched: false }
      ],
      stack: { slices: ['456'] }
    };
    instance.setState({ initialState });
    instance.onGoodMatch('123');
    expect(instance.state.initialState).toEqual(expectedState);
  });

  test('reset game and save default player name if not finshed editing', () => {
    const submitPlayerSpy = jest.spyOn(instance, 'submitPlayerName');
    const resetGameSpy = jest.spyOn(instance, 'resetGame');
    const leaders = [{ name: 'Tadeusz', editing: true, time: 37456 }];
    instance.setState({ leaders });

    instance.closeModal();
    expect(submitPlayerSpy).toHaveBeenCalledWith(DEFAULT_PLAYER_NAME);
    expect(resetGameSpy).toHaveBeenCalled();
    submitPlayerSpy.mockRestore();
    resetGameSpy.mockRestore();
  });

  test('reset game on modal close', () => {
    const submitPlayerSpy = jest.spyOn(instance, 'submitPlayerName');
    const resetGameSpy = jest.spyOn(instance, 'resetGame');
    const leaders = [{ name: 'Tadeusz', editing: false, time: 37456 }];
    instance.setState({ leaders });

    instance.closeModal();
    expect(submitPlayerSpy).not.toHaveBeenCalledWith(DEFAULT_PLAYER_NAME);
    expect(resetGameSpy).toHaveBeenCalled();
    submitPlayerSpy.mockRestore();
    resetGameSpy.mockRestore();
  });

  test('submit player name', () => {
    const setStateSpy = jest.spyOn(instance, 'setState');
    const saveToLocalStorageSpy = jest.spyOn(
      localStorageUtils,
      'saveToLocalStorage'
    );
    const leaders = [
      { name: null, editing: true, time: 37456 },
      { name: 'Lidiusz', editing: false, time: 49789 }
    ];
    const newName = 'Test';
    const expectedNewLeaders = [
      { name: newName, editing: false, time: 37456 },
      { name: 'Lidiusz', editing: false, time: 49789 }
    ];
    instance.setState({ leaders });

    instance.submitPlayerName(newName);

    expect(setStateSpy).toHaveBeenCalledWith({ leaders: expectedNewLeaders });
    expect(saveToLocalStorageSpy).toHaveBeenCalledWith(
      'leaders',
      expectedNewLeaders
    );
    setStateSpy.mockRestore();
  });

  afterAll(() => {
    ReactDOM.unmountComponentAtNode(div);
  });
});
