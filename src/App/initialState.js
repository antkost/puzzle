export const generateBoard = (columns, rows) => {
  const matrix = [];
  for (let columnIndex = 0; columnIndex < columns; columnIndex++) {
    for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
      const element = {
        id: `${columnIndex}-${rowIndex}`,
        column: columnIndex,
        row: rowIndex,
        matched: false
      };
      matrix.push(element);
    }
  }
  return matrix;
};

export const getSlices = board => {
  return board.map(cell => cell.id);
};

export const initialLeaders = [
  { name: 'Tadeusz', editing: false, time: 37456 },
  { name: 'Michalek', editing: false, time: 21789 },
  { name: 'Anton', editing: false, time: 14123 },
  { name: 'Lidiusz', editing: false, time: 49789 }
];
