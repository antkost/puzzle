import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import styled from 'styled-components';

import Timer from '../components/Timer';
import Modal from '../components/Modal';
import LeaderBoard, { DEFAULT_PLAYER_NAME } from '../components/LeaderBoard';
import CardsStack from '../components/CardsStack';
import Board from '../components/Board';
import { initialLeaders, generateBoard, getSlices } from './initialState';
import { boardWidth, boardHeight } from '../utils/picture.constants';
import {
  saveToLocalStorage,
  getFromLocalStorage
} from '../utils/localStorage.utils';

const Container = styled.div`
  margin: 50px 0;
`;

const BoardWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const board = generateBoard(boardWidth, boardHeight);
const slices = getSlices(board);
const leaders = getFromLocalStorage('leaders') || initialLeaders;

class App extends Component {
  state = {
    initialState: {
      board,
      stack: {
        slices
      }
    },
    leaders,
    isTimerOn: false,
    shouldAddTime: false,
    shouldResetTime: false,
    isModalShowing: false
  };

  onDragStart = () => {
    this.setState({ isTimerOn: true, shouldResetTime: false });
  };

  onDragEnd = result => {
    const { destination, draggableId, source } = result;

    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId) {
      return;
    }

    const dropCell = this.state.initialState.board.find(
      cell => cell.id === destination.droppableId
    );
    if (dropCell && dropCell.matched) {
      return;
    }

    if (destination.droppableId === draggableId) {
      this.onGoodMatch(destination.droppableId);
    } else {
      this.onBadMatch();
      return;
    }

    if (this.hasGameFinished()) {
      this.setState({ isTimerOn: false });
      this.openModal();
    }
  };

  hasGameFinished = () => {
    return this.state.initialState.board.every(cell => cell.matched);
  };

  onBadMatch = () => {
    this.setState({ shouldAddTime: true });
  };

  onGoodMatch = droppableId => {
    const { stack, board } = this.state.initialState;

    const slicesLeft = stack.slices.filter(slice => slice !== droppableId);

    const newBoard = board.map(cell => {
      if (cell.id === droppableId) {
        return {
          ...cell,
          matched: true
        };
      }
      return cell;
    });

    const newState = {
      ...this.state,
      initialState: {
        ...this.state.initialState,
        board: newBoard,
        stack: {
          slices: slicesLeft
        }
      }
    };

    this.setState(newState);
  };

  onTimeAdded = () => {
    this.setState({ shouldAddTime: false });
  };

  openModal = () => {
    this.setState({
      isModalShowing: true
    });
  };

  closeModal = () => {
    const { leaders } = this.state;
    const isInEditing = leaders.some(leader => leader.editing);

    if (isInEditing) {
      this.submitPlayerName(DEFAULT_PLAYER_NAME);
    }

    this.resetGame();
  };

  recordPlayer = time => {
    const player = { name: null, editing: true, time };
    const newLeaders = [...this.state.leaders, player];
    const sortedLeaders = newLeaders.sort((playerA, playerB) => {
      return playerA.time - playerB.time;
    });

    this.setState({ leaders: sortedLeaders });
  };

  submitPlayerName = name => {
    const newLeaders = this.state.leaders.map(leader => {
      if (leader.editing) {
        return {
          ...leader,
          editing: false,
          name
        };
      }

      return leader;
    });

    this.setState({ leaders: newLeaders });
    saveToLocalStorage('leaders', newLeaders);
  };

  resetGame = () => {
    const newBoard = generateBoard(boardWidth, boardHeight);

    this.setState({
      initialState: {
        board: newBoard,
        stack: {
          slices: getSlices(newBoard)
        }
      },
      isModalShowing: false,
      shouldResetTime: true
    });
  };

  render() {
    const {
      initialState: {
        board,
        stack: { slices }
      },
      isTimerOn,
      shouldAddTime,
      shouldResetTime,
      isModalShowing,
      leaders
    } = this.state;

    return (
      <Container>
        <Timer
          isTimerOn={isTimerOn}
          shouldAddTime={shouldAddTime}
          onTimeAdded={this.onTimeAdded}
          recordPlayer={this.recordPlayer}
          shouldResetTime={shouldResetTime}
        />

        <DragDropContext
          onDragStart={this.onDragStart}
          onDragEnd={this.onDragEnd}
        >
          <BoardWrapper>
            <Board board={board} isActive={isTimerOn} />
            <CardsStack slices={slices} board={board} />
          </BoardWrapper>
        </DragDropContext>

        <Modal show={isModalShowing} close={this.closeModal}>
          <LeaderBoard leaders={leaders} submit={this.submitPlayerName} />
        </Modal>
      </Container>
    );
  }
}

export default App;
